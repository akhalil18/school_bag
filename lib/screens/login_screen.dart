import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_bag/localization/app_translationss.dart';
import 'package:school_bag/utils/storage/shared_prefrance_manager.dart';
import 'package:school_bag/widgets/platform_scaffold_widget.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PlatformScaffoldWidget(
      appBar: AppBar(
        title: Text('Login'),
        centerTitle: true,
      ),
      cupertinoNavigationBar: CupertinoNavigationBar(
        middle: Text(AppTranslations.of(context).text('key_app_name')),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            CupertinoButton(
              child: Text('English'),
              onPressed: () {
                SharedPreferencesManager().saveLanguage('en');
              },
            ),
            CupertinoButton(
              child: Text('عربي'),
              onPressed: () {
                SharedPreferencesManager().saveLanguage('ar');
              },
            )
          ],
        ),
      ),
      drawer: Drawer(),
    );
  }
}
