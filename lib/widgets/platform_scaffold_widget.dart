import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_bag/screens/login_screen.dart';

class PlatformScaffoldWidget extends StatelessWidget {
  final AppBar appBar;
  final CupertinoNavigationBar cupertinoNavigationBar;
  final Widget body;
  final Widget drawer;
  final isHaveTabScaffold;

  PlatformScaffoldWidget(
      {this.appBar,
      this.cupertinoNavigationBar,
      this.body,
      this.drawer,
      this.isHaveTabScaffold = false});

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      return Scaffold(
        appBar: appBar,
        body: body,
        drawer: drawer,
        endDrawer: drawer,
      );
    } else if (Platform.isIOS) {
      return (!isHaveTabScaffold)
          ? CupertinoPageScaffold(
              navigationBar: cupertinoNavigationBar,
              child: body,
            )
          : CupertinoTabScaffold(
              tabBar: CupertinoTabBar(
                  backgroundColor: Colors.blue,
                  activeColor: Colors.white,
                  items: [
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.apps,
                        size: 26,
                      ),
                      title: Text('home'),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.gavel,
                        size: 26,
                      ),
                      title: Text('home'),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.work,
                        size: 26,
                      ),
                      title: Text('home'),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        CupertinoIcons.profile_circled,
                        size: 28,
                      ),
                      title: Text('home'),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        CupertinoIcons.ellipsis,
                        size: 28,
                      ),
                      title: Text('home'),
                    ),
                  ]),
              tabBuilder: (context, index) {
                if (index == 0) {
                  return CupertinoTabView(
                    builder: (_) {
                      return LoginScreen();
                    },
                  );
                } else if (index == 1) {
                  return CupertinoTabView(
                    builder: (_) {
                      return LoginScreen();
                    },
                  );
                } else if (index == 2) {
                  return CupertinoTabView(
                    builder: (_) {
                      return LoginScreen();
                    },
                  );
                } else if (index == 3) {
                  return CupertinoTabView(
                    builder: (_) {
                      return LoginScreen();
                    },
                  );
                } else {
                  return CupertinoTabView(builder: (_) {
                    return LoginScreen();
                  });
                }
              },
            );
    }
  }
}
