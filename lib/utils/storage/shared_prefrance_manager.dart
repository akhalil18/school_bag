import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import 'shared_preferences_keys.dart';

class SharedPreferencesManager {

  // Save Language
  Future saveLanguage(String language) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString(SharedPreferencesKeys.language.toString(), language);
  }

  // Get Language
  Future<String> getLanguage() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(SharedPreferencesKeys.language.toString()) ?? 'ar';
  }

//  // Save User
//  Future saveUser(User user) async {
//    SharedPreferences pref = await SharedPreferences.getInstance();
//    pref.setString(SharedPreferencesKeys.api_token.toString(), user.apiToken);
//    pref.setInt(SharedPreferencesKeys.user_id.toString(), user.userId);
//    pref.setString(SharedPreferencesKeys.email.toString(), user.email);
//    pref.setString(SharedPreferencesKeys.image.toString(), user.image);
//    pref.setString(SharedPreferencesKeys.name.toString(), user.name);
//    pref.setString(SharedPreferencesKeys.phone.toString(), user.phone);
//    pref.setString(SharedPreferencesKeys.gender.toString(), user.gender);
//    pref.setInt(SharedPreferencesKeys.country_id.toString(), user.countryId);
//    pref.setString(SharedPreferencesKeys.subscribe_end.toString(), user.subscribeEnd);
//    pref.setString(SharedPreferencesKeys.code.toString(), user.code);
//    pref.setInt(SharedPreferencesKeys.total_points.toString(), user.totalPoints);
//    pref.setBool(SharedPreferencesKeys.isLogged.toString(), true);
//
//    // for notification
//    saveCountryId(user.countryId);
//  }

//  // Get User
//  Future<User> getUser() async {
//    SharedPreferences pref = await SharedPreferences.getInstance();
//    return User(
//        apiToken: pref.getString(SharedPreferencesKeys.api_token.toString()),
//      userId: pref.getInt(SharedPreferencesKeys.user_id.toString()),
//      email: pref.getString(SharedPreferencesKeys.email.toString()),
//      image: pref.getString(SharedPreferencesKeys.image.toString()),
//      name: pref.getString(SharedPreferencesKeys.name.toString()),
//      phone: pref.getString(SharedPreferencesKeys.phone.toString()),
//      gender: pref.getString(SharedPreferencesKeys.gender.toString()),
//      countryId: pref.getInt(SharedPreferencesKeys.country_id.toString()),
//      subscribeEnd: pref.getString(SharedPreferencesKeys.subscribe_end.toString()),
//      code: pref.getString(SharedPreferencesKeys.code.toString()),
//      totalPoints: pref.getInt(SharedPreferencesKeys.total_points.toString()),
//    );
//  }

//  // Clear User (Logout)
//  logoutUser() async {
//    SharedPreferences pref = await SharedPreferences.getInstance();
//    pref.remove(SharedPreferencesKeys.api_token.toString());
//    pref.remove(SharedPreferencesKeys.user_id.toString());
//    pref.remove(SharedPreferencesKeys.email.toString());
//    pref.remove(SharedPreferencesKeys.image.toString());
//    pref.remove(SharedPreferencesKeys.name.toString());
//    pref.remove(SharedPreferencesKeys.phone.toString());
//    pref.remove(SharedPreferencesKeys.gender.toString());
//    pref.remove(SharedPreferencesKeys.country_id.toString());
//    pref.remove(SharedPreferencesKeys.subscribe_end.toString());
//    pref.remove(SharedPreferencesKeys.code.toString());
//    pref.remove(SharedPreferencesKeys.total_points.toString());
//    pref.setBool(SharedPreferencesKeys.isLogged.toString(), false);
//  }

//  // check if loggedIn
//  Future<bool> isLoggedIn() async {
//    SharedPreferences pref = await SharedPreferences.getInstance();
//    return pref.getBool(SharedPreferencesKeys.isLogged.toString()) ?? false;
//  }

}
