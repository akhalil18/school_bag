import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:school_bag/screens/login_screen.dart';

import 'localization/app_translations_delegate.dart';
import 'localization/application.dart';
import 'utils/storage/shared_prefrance_manager.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PlatformAppWidget();
  }
}

class PlatformAppWidget extends StatefulWidget {
  @override
  _PlatformAppWidgetState createState() => _PlatformAppWidgetState();
}

class _PlatformAppWidgetState extends State<PlatformAppWidget> {

  //////////Language/////////
  AppTranslationsDelegate _newLocaleDelegate;
  String lang = 'ar';

  @override
  void initState() {
    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }

  // languages
  final supportedLocales = [
    const Locale("en", ""),
    const Locale("ar", ""),
  ];

  @override
  Widget build(BuildContext context) {
    SharedPreferencesManager().getLanguage().then((langValue) {
      setState(() {
        lang = langValue;
      });
    });

    if (Platform.isAndroid) {  // for android
      return MaterialApp(
        initialRoute: '/',
        routes: routes,
        // language
        localizationsDelegates: [
          GlobalCupertinoLocalizations.delegate,
          _newLocaleDelegate,
          //provides localised strings
          GlobalMaterialLocalizations.delegate,
          //provides RTL support
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: supportedLocales,
        locale: Locale(lang, ''),
      );
    } else if (Platform.isIOS) {  // for ios
      return CupertinoApp(
        initialRoute: '/',
        routes: routes,
        // language
        localizationsDelegates: [
          GlobalCupertinoLocalizations.delegate,
          _newLocaleDelegate,
          //provides localised strings
          GlobalMaterialLocalizations.delegate,
          //provides RTL support
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: supportedLocales,
        locale: Locale(lang, ''),
      );
    }
  }

  final routes = {
    '/': (_) => LoginScreen(),
  };
}
